import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import torch.nn.functional as F
import torch.utils.data as data
import torch.optim as optim
import numpy as np
from PIL import Image
import random

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
epochs=10

print(f'Working on device={device}')
def get_transform():
    mean = [0.485, 0.456, 0.406]
    std = [0.229, 0.224, 0.225]

    return transforms.Compose([
        transforms.ToTensor()
    ])

class Net(nn.Module):  # basic block for Conv2d
    def __init__(self, in_chanel=1, out_chanel=6):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(
            in_chanel, out_chanel, kernel_size=3, stride=1, padding=1)
        self.bn1 = nn.BatchNorm2d(6)
        self.conv2 = nn.Conv2d(
            18, 32, kernel_size=3, stride=1, padding=1)
        self.bn2 = nn.BatchNorm2d(32)
        self.conv3 =nn.Conv2d(
            32, 1, kernel_size=3, stride=1, padding=1)
        self.bn3= nn.BatchNorm2d(1)
        self.shortcut = nn.Sequential()

    def forward(self, left, right, center):
        outL = F.relu(self.bn1(self.conv1(left)))
        outR = F.relu(self.bn1(self.conv1(right)))
        outC = F.relu(self.bn1(self.conv1(center)))
        out= torch.cat((outL,outR,outC),1)
        out = self.bn2(self.conv2(out))
        out = F.relu(out)
        out = self.bn3(self.conv3(out))
        #out += self.shortcut(x)
        out = F.relu(out)
        return out

class myImageFloder(data.Dataset):
    def __init__(self, left, right, center, original, training):

        self.left = left
        self.right = right
        self.center = center
        self.original = original
        self.training = training

    def __getitem__(self, index):
        left = self.left[index]
        right = self.right[index]
        center = self.center[index]
        original = self.original[index]

        left_img = Image.open(left)
        right_img = Image.open(right)
        center_img = Image.open(center)
        original_img= Image.open(original)
    
        original_img=np.asarray(original_img, dtype=np.float)

        if self.training:
            w, h = left_img.size
            th, tw = 256, 512

            x1 = random.randint(0, w - tw)
            y1 = random.randint(0, h - th)

            left_img = left_img.crop((x1, y1, x1 + tw, y1 + th))
            right_img = right_img.crop((x1, y1, x1 + tw, y1 + th))
            center_img= center_img.crop((x1, y1, x1 + tw, y1 + th))

            original_img = original_img[y1:y1 + th, x1:x1 + tw]

            processed = get_transform()
            left_img = processed(left_img).float()
            right_img = processed(right_img).float()
            center_img = processed(center_img).float()

            return left_img, right_img, center_img, original_img
        else:
            processed =get_transform()
            left_img = processed(left_img).numpy()
            right_img = processed(right_img).numpy()
            center_img=processed(center_img).numpy()
            return left_img, right_img, center_img, original_img

    def __len__(self):
        return len(self.left)

all_left_img = ["123.png", "123.png"]
all_right_img = ["123.png", "123.png"]
all_center_img = ["123.png", "123.png"]
all_original_img=["123.png", "123.png"]
test_left_img = ["123.png", "123.png"]
test_right_img = ["123.png", "123.png"]
test_center_img = ["123.png", "123.png"]
test_original_img= ["123.png", "123.png"]

TrainImgLoader = torch.utils.data.DataLoader(
    myImageFloder(all_left_img, all_right_img, all_center_img,all_original_img, True),
    batch_size=12, shuffle=True, num_workers=8, drop_last=False)

TestImgLoader = torch.utils.data.DataLoader(
    myImageFloder(test_left_img, test_right_img, test_center_img, test_original_img, False),
    batch_size=8, shuffle=False, num_workers=4, drop_last=False)

def train():
    model = Net()
    model.to(device)
    optimizer = optim.Adam(model.parameters(), lr=0.001)
    for epoch in range(epochs):
        for batch_idx, (imgL_crop, imgR_crop, imgC_crop, img_original) in enumerate(TrainImgLoader):
            if torch.cuda.is_available():
                imgL, imgR, imgC, img_original= imgL_crop.cuda(), imgR_crop.cuda(), imgC_crop.cuda(), img_original.cuda()
            optimizer.zero_grad()
            output = model(imgL, imgR, imgC)
            output = torch.squeeze(output, 1)
            optimizer.step()
    torch.save({
        'epoch': epoch,
        'state_dict': model.state_dict(),
    }, "model.tar")

if __name__=="__main__" :
    train()